﻿using DotNet.Testcontainers.Builders;
using DotNet.Testcontainers.Configurations;
using DotNet.Testcontainers.Containers;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Moq;
using Npgsql;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Ucas.ApiPostgres.Template.Api;
using Xunit;

namespace Ucas.ApiPostgres.Template.IntegrationTests.Api
{
    [CollectionDefinition(ApiIntegrationTestFixture.CollectionName)]
    public class ApiTestCollection : ICollectionFixture<ApiIntegrationTestFixture>
    {
        // This class has no code, and is never created. Its purpose is simply
        // to be the place to apply [CollectionDefinition] and all the
        // ICollectionFixture<> interfaces.

        // https://xunit.net/docs/shared-context#collection-fixture
    }

    public class ApiIntegrationTestFixture : WebApplicationFactory<Startup>, IAsyncLifetime
    {
        public const string CollectionName = "API Tests";

        public NpgsqlConnectionStringBuilder ApiUserConnectionStringBuilder;
        public NpgsqlConnectionStringBuilder ReadOnlyUserConnectionStringBuilder;
        public NpgsqlConnectionStringBuilder MigratorUserConnectionStringBuilder;
        private PostgreSqlTestcontainer _testContainer;

        public Mock<IDiagnosticContext> MockDiagnosticContext;

        public async Task InitializeAsync()
        {
            Environment.SetEnvironmentVariable("ASPNETCORE_ENVIRONMENT", "Development");

            _testContainer = new TestcontainersBuilder<PostgreSqlTestcontainer>()
                .WithName($"Test_{Guid.NewGuid()}")
                .WithDatabase(new PostgreSqlTestcontainerConfiguration
                {
                    Database = "db",

                    // matching standard ucas local pgsql user and password
                    // to make it easy to remember
                    Username = "postgres",
                    Password = "postgrespwd"
                })
                .Build();

            await _testContainer.StartAsync();
        }

        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            Environment.SetEnvironmentVariable("ASPNETCORE_ENVIRONMENT", "Development");

            builder.ConfigureAppConfiguration(config =>
            {
                var apiUserConnection = new NpgsqlConnectionStringBuilder(_testContainer.ConnectionString)
                {
                    Username = "api_user",
                    Password = "api_pwd",
                    Database = "test_service"
                };
                ApiUserConnectionStringBuilder = apiUserConnection;


                var readOnlyUserConnection = new NpgsqlConnectionStringBuilder(_testContainer.ConnectionString)
                {
                    Username = "ro_user",
                    Password = "ro_pwd",
                    Database = "test_service"
                };
                ReadOnlyUserConnectionStringBuilder = readOnlyUserConnection;

                var migratorConnector = new NpgsqlConnectionStringBuilder(_testContainer.ConnectionString)
                {
                    Database = "test_service"
                };
                MigratorUserConnectionStringBuilder = migratorConnector;

                config.AddInMemoryCollection(new Dictionary<string, string>
                {
                    ["ConnectionStrings:db"] = apiUserConnection.ConnectionString,
                    ["ConnectionStrings:db_Migrator"] = migratorConnector.ConnectionString,
                    ["ConnectionStrings:db_RO"] = readOnlyUserConnection.ConnectionString
                });
            });

            // Fix for https://jira.ucas.ac.uk/browse/RD-361 where these were failing in bamboo
            builder.UseContentRoot(Directory.GetCurrentDirectory());

            builder.ConfigureServices(services =>
            {
                MockDiagnosticContext = new Mock<IDiagnosticContext>();
                MockDiagnosticContext.SetupAllProperties();

                services.RemoveAll<IDiagnosticContext>();
                services.AddSingleton(MockDiagnosticContext.Object);

                services.RemoveAll<IMemoryCache>();
                services.AddSingleton<IMemoryCache>(new MemoryCache(new MemoryCacheOptions()));

                var serviceProvider = services.BuildServiceProvider();
                using var scope = serviceProvider.CreateScope();
            });
        }

        public void ResetMockDiagnosticContext()
        {
            MockDiagnosticContext.Reset();
            MockDiagnosticContext.SetupAllProperties();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
                Task.WaitAll(DisposeAsync());

            base.Dispose(disposing);
        }

#pragma warning disable 0114
        public async Task DisposeAsync()
        {
            await _testContainer.DisposeAsync();
        }
    }
}
