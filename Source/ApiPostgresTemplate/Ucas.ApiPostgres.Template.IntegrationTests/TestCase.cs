﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ucas.ApiPostgres.Template.Db.Context;
using Ucas.ApiPostgres.Template.Tests.Helpers;

namespace Ucas.ApiPostgres.Template.IntegrationTests
{
    public class TestCase : IAsyncDisposable, IDisposable
    {
        private bool _disposed = false;
        private List<object> _entities;

        private IServiceProvider _serviceProvider;
        private IServiceScope _serviceScope;

        public Db.Context.ApplicationContext DbContext;
        public TestCase(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
            _entities = new List<object>();

            RefreshScope();
            SeedTestData();
        }

        public TestCase RefreshScope()
        {
            _serviceScope = _serviceProvider.CreateScope();
            DbContext = _serviceScope.ServiceProvider.GetRequiredService<ApplicationContext>();

            return this;
        }

        public TestCase(ApplicationContext dbContext)
        {
            DbContext = dbContext;

            SeedTestData();
        }

        public async Task<T> GetAsync<T>(Func<Db.Context.ApplicationContext, Task<T>> accessor)
        {
            var entity = await accessor(DbContext);

            if (entity is IEnumerable<object> entities)
            {
                _entities.AddRange(entities);
            }
            else
            {
                _entities.Add(entity);
            }

            return entity;
        }

        public async Task AddToDbContextAsync(List<object> entities)
        {
            _entities.AddRange(entities);

            DbContext.AddRange(entities);
            await DbContext.SaveChangesAsync();
        }

        public void AddToDbContext(List<object> entities)
        {
            _entities.AddRange(entities);

            DbContext.AddRange(entities);
            DbContext.SaveChanges();
        }

        public void AddToDbContext(object entity)
        {
            _entities.Add(entity);

            DbContext.Add(entity);
            DbContext.SaveChanges();
        }

        public async Task AddToDbContextAsync(params object[] entities) =>
            await AddToDbContextAsync(entities.ToList());

        private void SeedTestData()
        {
            var dummyData = DataSeedHelper.SeedTestData(DbContext);
            _entities.AddRange(dummyData);
            DbContext.AttachRange(dummyData);
        }

        public async ValueTask DisposeAsync()
        {
            await DisposeAsyncCore();

            Dispose(false);
            GC.SuppressFinalize(this);
        }

        protected virtual async ValueTask DisposeAsyncCore()
        {
            DbContext?.RemoveRange(_entities);
            await DbContext?.SaveChangesAsync();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
            {
                return;
            }

            if (disposing)
            {
                DbContext?.RemoveRange(_entities);
                DbContext?.SaveChanges();
            }

            _serviceScope?.Dispose();
            _disposed = true;
        }
    }
}
