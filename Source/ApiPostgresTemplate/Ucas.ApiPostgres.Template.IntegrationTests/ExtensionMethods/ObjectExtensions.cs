using System.Net.Http;
using System.Net.Mime;
using System.Text;

namespace Ucas.ApiPostgres.Template.IntegrationTests.ExtensionMethods
{
    public static class ObjectExtensions
    {
        public static StringContent CreateRequestBody(this object request) =>
            (request == null ? null : new StringContent(request.ToJsonString(), Encoding.UTF8, MediaTypeNames.Application.Json))!;
    }
}
