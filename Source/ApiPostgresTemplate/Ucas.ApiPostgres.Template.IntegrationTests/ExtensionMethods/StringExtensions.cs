using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Ucas.ApiPostgres.Template.IntegrationTests.ExtensionMethods
{
    public static class StringExtensions
    {
        /// <summary>
        /// Serializes an object to a JSON string
        /// </summary>
        /// <param name="target">An <see cref="object"/> to serialize</param>
        /// <param name="formatting">The desired <see cref="System.Xml.Formatting"/></param>
        /// <returns>A JSON <see cref="string"/> of the serialized object or an empty string if it fails</returns>
        public static string ToJsonString(this object target, Formatting formatting = Formatting.Indented)
        {
            try
            {
                return JsonConvert.SerializeObject(target, formatting, new JsonSerializerSettings
                {
                    Formatting = formatting,
                    ContractResolver = new CamelCasePropertyNamesContractResolver(),
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                });
            }
            catch
            {
                return string.Empty;
            }
        }
    }
}
