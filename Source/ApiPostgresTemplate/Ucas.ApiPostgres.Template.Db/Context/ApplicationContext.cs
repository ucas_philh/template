﻿using Microsoft.EntityFrameworkCore;
using Ucas.ApiPostgres.Template.Db.Entities;

namespace Ucas.ApiPostgres.Template.Db.Context
{
    public class ApplicationContext : DbContext
    {
        public ApplicationContext() { }

        public ApplicationContext(DbContextOptions<ApplicationContext> options)
            : base(options) { }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
 
        }
    }
}
