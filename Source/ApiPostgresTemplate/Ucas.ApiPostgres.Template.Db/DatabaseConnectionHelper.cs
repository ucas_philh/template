﻿using Microsoft.Extensions.Configuration;
using Npgsql;

namespace Ucas.ApiPostgres.Template.Db
{
    public static class DatabaseConnectionHelper
    {
        public static string GetConnectionString(IConfiguration config)
        {
            string connectionString;

            if (config["ASPNETCORE_ENVIRONMENT"] == "Development")
            {
                connectionString = config["ConnectionStrings:db"];
            }
            else
            {
                var secretName = config["SecretsManagerApiUserCredentials"];

                var port = config.GetValue<int>($"{secretName}:port");
                var hostname = config.GetValue<string>($"{secretName}:host");
                var apiUsername = config.GetValue<string>($"{secretName}:username").ToLower();
                var apiPassword = config.GetValue<string>($"{secretName}:password");

                // Connection string builder for database and API User
                var connectionStringBuilder =
                    new NpgsqlConnectionStringBuilder()
                    {
                        Host = hostname,
                        Port = port,
                        Username = apiUsername,
                        Database = config["DatabaseName"],
                        Password = apiPassword,
                        MaxPoolSize = int.Parse(config["MaxConnectionPoolSize"] ?? "100")
                    };

                connectionString = connectionStringBuilder.ConnectionString;
            }

            return connectionString;
        }
    }
}
