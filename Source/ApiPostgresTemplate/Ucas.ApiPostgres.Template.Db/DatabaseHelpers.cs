﻿using Microsoft.Extensions.Configuration;
using Npgsql;
using System;

namespace Ucas.ApiPostgres.Template.Db
{
    public static class DatabaseHelpers
    {
        /// <summary>
        ///     Setup the required connection strings, create the database, create the api and read only user set their permissions.
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="stackName"></param>
        /// <param name="databaseName"></param>
        /// <returns>Connection string to use for creating the migration context.</returns>
        public static string CreateApplicationDatabaseAndUser(IConfiguration configuration, string stackName, string databaseName)
        {
            // The secret names are hardcoded into the template so we're fine to hard code them here.
            var port = configuration.GetValue<int>($"{stackName}-ChangeMeRDSRootUserCredentials:port");
            var hostname = configuration.GetValue<string>($"{stackName}-ChangeMeRDSRootUserCredentials:host");

            // Postgres usernames are always lowercased, so change to lower just in case they been entered into bamboo in upper.
            var rootUsername = configuration.GetValue<string>($"{stackName}-ChangeMeRDSRootUserCredentials:username").ToLower();
            var rootPassword = configuration.GetValue<string>($"{stackName}-ChangeMeRDSRootUserCredentials:password");

            var apiUsername = configuration.GetValue<string>($"{stackName}-ChangeMeRDSApiUserCredentials:username").ToLower();
            var apiPassword = configuration.GetValue<string>($"{stackName}-ChangeMeRDSApiUserCredentials:password");

            var readonlyUsername = configuration.GetValue<string>($"{stackName}-ChangeMeRDSReadOnlyUserCredentials:username").ToLower();
            var readonlyPassword = configuration.GetValue<string>($"{stackName}-ChangeMeRDSReadOnlyUserCredentials:password");

            // Connection string builder for the default user and database. You connect to this when creating users and new DBs
            var postgresConnectionStringBuilder =
                new NpgsqlConnectionStringBuilder()
                {
                    Host = hostname,
                    Port = port,
                    Username = rootUsername,
                    Database = "postgres",
                    Password = rootPassword,
                    MaxPoolSize = 100,
                    Timeout = 180, // larger timeout to deal with auto pause in Dev.
                    CommandTimeout = 180 // larger timeout to deal with auto pause in Dev.
                };

            // Connection string builder for our database and API User
            var ConnectionStringBuilder =
                new NpgsqlConnectionStringBuilder()
                {
                    Host = hostname,
                    Port = port,
                    Username = apiUsername,
                    Database = databaseName,
                    Password = apiPassword,
                    MaxPoolSize = 100
                };

            // Connection string builder for our database read only user
            var readOnlyConnectionStringBuilder =
                new NpgsqlConnectionStringBuilder()
                {
                    Host = hostname,
                    Port = port,
                    Username = readonlyUsername,
                    Database = databaseName,
                    Password = readonlyPassword,
                    MaxPoolSize = 100
                };

            // Connection string builder for the migrations to run against.
            var migrationConnectionStringBuilder =
                new NpgsqlConnectionStringBuilder()
                {
                    Host = hostname,
                    Port = port,
                    Username = rootUsername,
                    Database = databaseName,
                    Password = rootPassword,
                    MaxPoolSize = 100,
                    Timeout = 180, // larger timeout to deal with auto pause in Dev.
                    CommandTimeout = 180 // larger timeout to deal with auto pause in Dev.
                };

            CreateApplicationDatabaseAndUser(ConnectionStringBuilder, readOnlyConnectionStringBuilder, migrationConnectionStringBuilder, postgresConnectionStringBuilder.ConnectionString);

            return migrationConnectionStringBuilder.ConnectionString;
        }

        public static void CreateApplicationDatabaseAndUser(NpgsqlConnectionStringBuilder connectionStringBuilder,
            NpgsqlConnectionStringBuilder readOnlyConnectionStringBuilder,
            NpgsqlConnectionStringBuilder migrationConnectionStringBuilder, string postgresConnectionString)
        {
            bool isFirstRun;
            using (var dbConnection = new NpgsqlConnection(postgresConnectionString))
            {
                dbConnection.Open();

                isFirstRun = CheckIfInitialSystemStartup(connectionStringBuilder.Database!, dbConnection);

                if (isFirstRun)
                {
                    CreateDatabaseAndApiUser(connectionStringBuilder, dbConnection);
                }
                else
                {
                    Console.WriteLine("Database and users have been created previously. Proceeding to migrations");
                }
            }

            if (isFirstRun)
            {
                using var dbConnection = new NpgsqlConnection(migrationConnectionStringBuilder.ConnectionString);
                dbConnection.Open();

                SetupUserPrivileges(connectionStringBuilder, dbConnection);

                CreateReadOnlyUser(readOnlyConnectionStringBuilder, dbConnection);
            }
        }

        private static void PostgreSqlExecuteNonQuery(string sql, NpgsqlConnection conn,
            NpgsqlConnectionStringBuilder connectionStringBuilder)
        {
            using var cmdSetup = new NpgsqlCommand(sql, conn);
            var cmdText = string.IsNullOrEmpty(connectionStringBuilder.Password)
                ? cmdSetup.CommandText
                : cmdSetup.CommandText.RedactPassword(connectionStringBuilder.Password);
            Console.WriteLine($"Running DB command {cmdText}");
            cmdSetup.ExecuteNonQuery();
        }

        private static bool CheckIfInitialSystemStartup(string dbName, NpgsqlConnection dbConnection)
        {
            if (string.IsNullOrEmpty(dbName))
                throw new ArgumentNullException(nameof(dbName));

            // Yes, datname is correct
            using var cmdCheck = new NpgsqlCommand("SELECT COUNT(*) FROM pg_database WHERE datname = @dbName", dbConnection);

            cmdCheck.Parameters.AddWithValue("dbName", dbName);

            Console.WriteLine($"Running DB command {cmdCheck.CommandText}");

            var result = cmdCheck.ExecuteScalar()?.ToString();
            if (string.IsNullOrEmpty(result))
                throw new Exception($"Db command failed: {cmdCheck.CommandText}");

            return (int.Parse(result) == 0);
        }

        private static void CreateDatabaseAndApiUser(NpgsqlConnectionStringBuilder applicationConnectionStringBuilder,
            NpgsqlConnection dbConnection)
        {
            Console.WriteLine($"Database {applicationConnectionStringBuilder.Database} was not found, will now create the database and the user");

            // Setup the application user and create the database. DDL statements can't be parameterised as you can only parameterise values for Insert, Select etc.
            var commands = new[]
            {
                $@"CREATE USER {applicationConnectionStringBuilder.Username} WITH PASSWORD '{applicationConnectionStringBuilder.Password}';",
                $@"CREATE DATABASE {applicationConnectionStringBuilder.Database};",
                $@"REVOKE ALL ON DATABASE {applicationConnectionStringBuilder.Database} FROM public;",
                $@"GRANT CONNECT ON DATABASE {applicationConnectionStringBuilder.Database} TO {applicationConnectionStringBuilder.Username};",
            };

            Array.ForEach(commands,
                sqlCommand =>
                    PostgreSqlExecuteNonQuery(sqlCommand, dbConnection, applicationConnectionStringBuilder));

            Console.WriteLine($"Successfully created database user {applicationConnectionStringBuilder.Username} and database {applicationConnectionStringBuilder.Database}");
        }

        private static void SetupUserPrivileges(NpgsqlConnectionStringBuilder applicationConnectionStringBuilder,
            NpgsqlConnection dbConnection)
        {
            var commands = new[]{
                "REVOKE CREATE ON SCHEMA public FROM public;",
                // give access to future tables and sequences
                $@"ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT SELECT, UPDATE, INSERT, DELETE ON TABLES TO {applicationConnectionStringBuilder.Username};",
                $@"ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT SELECT, USAGE ON SEQUENCES TO {applicationConnectionStringBuilder.Username};"
            };

            Array.ForEach(commands, sqlCommand => PostgreSqlExecuteNonQuery(sqlCommand, dbConnection, applicationConnectionStringBuilder));

            Console.WriteLine($"Successfully set privileges for database user {applicationConnectionStringBuilder.Username}");
        }

        private static void CreateReadOnlyUser(NpgsqlConnectionStringBuilder readOnlyConnectionStringBuilder,
            NpgsqlConnection dbConnection)
        {
            Console.WriteLine($"About to create read only user {readOnlyConnectionStringBuilder.Username} and set its privileges");

            // DDL statements can't be parameterised as you can only parameterise values for Insert, Select etc.
            var commands = new[]
            {
                $@"CREATE USER {readOnlyConnectionStringBuilder.Username} WITH PASSWORD '{readOnlyConnectionStringBuilder.Password}';",
                $@"GRANT CONNECT ON DATABASE {readOnlyConnectionStringBuilder.Database} TO {readOnlyConnectionStringBuilder.Username};",
                // give access to future tables and sequences
                $@"ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT SELECT ON TABLES TO {readOnlyConnectionStringBuilder.Username};",
                $@"ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT SELECT ON SEQUENCES TO {readOnlyConnectionStringBuilder.Username};"
            };

            Array.ForEach(commands, sqlCommand => PostgreSqlExecuteNonQuery(sqlCommand, dbConnection, readOnlyConnectionStringBuilder));

            Console.WriteLine($"Successfully created database user {readOnlyConnectionStringBuilder.Username} with read only privileges");
        }

        private static string RedactPassword(this string message, string password) =>
            message.Replace(password, "***********");

        public static string GetUnprocessedConnectionString(IConfiguration configuration, string connectionStringName) =>
            configuration.GetConnectionString(connectionStringName) ??
                   throw new Exception($"Cannot get connection string {connectionStringName} - no config value found");

    }
}
