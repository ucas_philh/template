Param
(
    [Parameter(Mandatory = $false)]  [switch] $SkipTests = $false,
    [Parameter(Mandatory = $false)]  [switch] $RunDockerTests = $false
)

Set-StrictMode -Version Latest -Verbose
$ErrorActionPreference = "stop"

function Write-Parameters {
    Write-Host "|Resolved Parameters:"
    Write-Host "+-----------------------------------------------------------------------------------"
    Write-Host "|         SKIP_TESTS: $SkipTests"
    Write-Host "|   RUN_DOCKER_TESTS: $RunDockerTests"
    Write-Host "+-----------------------------------------------------------------------------------"
}

function ToolsInfo {
    Write-Host "| TOOLS INFO"
    dotnet --info
    if ($LASTEXITCODE -ne 0) { throw 'dotnet failed' }
    dotnet tool update amazon.lambda.tools --tool-path $pwd/tool
    if ($LASTEXITCODE -ne 0) { throw 'install/update aws lambda tools failed' }
    Write-Host "+----------------------------------------------------------------------------------------------"
}

function Clean {
    Write-Host "| CLEAN"

    if (Test-Path .\NugetPackage) {
        Remove-Item -Recurse -Force .\NugetPackage
    }
    New-Item -ItemType Directory .\NugetPackage

    if (Test-Path .\Publish) {
        Remove-Item -Recurse -Force .\Publish
    }
    New-Item -ItemType Directory .\Publish

    Remove-Item -Recurse -Force **\bin
    Remove-Item -Recurse -Force **\obj
    Remove-Item -Recurse -Force **\Publish

    dotnet clean .\ucas.providers.management.sln -c Release
    if ($LASTEXITCODE -ne 0) { throw 'Clean failed' }
    Write-Host "+----------------------------------------------------------------------------------------------"
}

function RestoreAndBuild {
    Write-Host "| BUILD"
    $copyright = "Copyright $([char]0x00a9) $(Get-Date -Format "yyyy"). All rights reserved."
    $company = "UCAS"

    dotnet build .\ucas.providers.management.sln -c Release /p:Copyright=$copyright /p:Company=$company
    if ($LASTEXITCODE -ne 0) { throw 'Build failed' }
    Write-Host "+----------------------------------------------------------------------------------------------"
}

function PublishDbMigrator {
    Write-Host "| PUBLISH APP"

    dotnet publish -v n .\Ucas.Providers.Management.Db.MigratorApp\Ucas.Providers.Management.Db.MigratorApp.csproj -c Release --no-build -o .\Ucas.Providers.Management.Db.MigratorApp\Publish
    Write-Host "|   Creating migrator archive"
    Compress-Archive .\Ucas.Providers.Management.Db.MigratorApp\Publish\* .\Publish\migrator.zip
    if ($LASTEXITCODE -ne 0) { throw 'Publish failed' }
    Write-Host "+----------------------------------------------------------------------------------------------"
}

function PublishMainLambda {
    Write-Host "| PUBLISH MAIN LAMBDA"
    Write-Host "|   Creating archives"
    ./tool/dotnet-lambda package -pl Ucas.Providers.Management.Api -o Publish\Ucas-Providers-Management-Api.zip
    if ($LASTEXITCODE -ne 0) { throw "Pack of Lambda failed" }

    Write-Host "+----------------------------------------------------------------------------------------------"
}

function PublishMessageConsumerLambda {
    Write-Host "| PUBLISH Message Consumer Lambda"
    Write-Host "| Creating archives"
    ./tool/dotnet-lambda package -pl Ucas.Providers.Management.Messaging.Consumer -o Publish\Ucas-Providers-Management-Message-Consumer.zip
    if ($LASTEXITCODE -ne 0) { throw "Pack of Lambda failed" }

    Write-Host "+----------------------------------------------------------------------------------------------"
}

function DiscoverProjects {
    Write-Host "| DISCOVER PROJECTS"
    $projects = @()
    Get-ChildItem -Filter *.csproj -Recurse |

    ForEach-Object {
        [xml]$project = Get-Content $_.FullName

        $isNuget = $null -ne $project.SelectSingleNode('/Project/PropertyGroup/GeneratePackageOnBuild[text()="True"]')
        $isLambda = $null -ne $project.SelectSingleNode('/Project/PropertyGroup/AWSProjectType[text()="Lambda"]')

        $projects += [System.Tuple]::Create($_.BaseName, $isNuget, $isLambda, $_.FullName)
    }

    $projects.ForEach{
        Write-Host "Project: [" $_.Item1 "] Is Nuget? [" $_.Item2 "] Is Lambda? [" $_.Item3 "]"
    }

    Write-Host "+----------------------------------------------------------------------------------------------"
    return , $projects
}

function UnitTest {
    Write-Host "| UNIT_TEST"
    if ($SkipTests) {
        Write-Host "  SKIPPED"
    }
    else {
        $csproj = ".\Ucas.Providers.Management.UnitTests\Ucas.Providers.Management.UnitTests.csproj"
        if ($RunDockerTests) {
            dotnet test $csproj -c Release --no-build --logger="trx"
        }
        else {
            dotnet test $csproj -c Release --no-build --logger="trx" --filter "RequiresDocker!=true"
        }
        if ($LASTEXITCODE -ne 0) { throw 'UnitTest failed' }
    }
    Write-Host "+----------------------------------------------------------------------------------------------"
}

function PublishProvidersManagementApiTests {
    Write-Host "| PUBLISH API Tests"

    dotnet publish -v n .\Ucas.Providers.Management.ApiTests\Ucas.Providers.Management.ApiTests.csproj -c Release --no-build -o .\Ucas.Providers.Management.ApiTests\Publish
    if ($LASTEXITCODE -ne 0) { throw 'Publish failed' }
    Write-Host "+----------------------------------------------------------------------------------------------"
}

function PublishMessageConsumerIntegrationTests {
    Write-Host "| PUBLISH_MESSAGE_CONSUMER_INTEGRATION_TESTS"

    # Integration tests require Docker and this script is running on a Windows Bamboo agent, which means we can't run them here.
    # Instead, we package up the bits we need into a directory, from where they can be picked up as an Artifact by a subsequent job that runs on a Linux agent.
    New-Item -Force -ItemType Directory .\Ucas.Providers.Management.Messaging.ConsumerTests\Publish\MessageConsumerIntegrationTests

    # Binaries from Ucas.Providers.Management.Messaging.ConsumerTests:
    # we can't `dotnet publish` these as that strips out things we need, like Ucas.Providers.Management.Messaging.ConsumerTests.deps.json,
    # so instead we'll look in bin\Release, where there should be a single net[n] folder (e.g. net6.0)
    Copy-Item "$((Get-ChildItem .\Ucas.Providers.Management.Messaging.ConsumerTests\bin\Release\net*)[0].FullName)\*" `
        .\Ucas.Providers.Management.Messaging.ConsumerTests\Publish\MessageConsumerIntegrationTests -Recurse

    # Note: there is no web API in the subject under test, so there should be no issue with having missing content files

    Write-Host "+----------------------------------------------------------------------------------------------"
}

function PublishProvidersManagementIntegrationTests {
    Write-Host "| PUBLISH_PROVIDERS_MANAGEMENT_INTEGRATION_TESTS"

    # Integration tests require Docker and this script is running on a Windows Bamboo agent, which means we can't run them here.
    # Instead, we package up the bits we need into a directory, from where they can be picked up as an Artifact by a subsequent job that runs on a Linux agent.
    New-Item -Force -ItemType Directory .\Ucas.Providers.Management.IntegrationTests\Publish\ProvidersManagementIntegrationTests

    # Binaries from Ucas.Providers.Management.IntegrationTests:
    # we can't `dotnet publish` these as that strips out things we need, like Ucas.Providers.Management.IntegrationTests.deps.json,
    # so instead we'll look in bin\Release, where there should be a single net[n] folder (e.g. net6.0)
    Copy-Item "$((Get-ChildItem .\Ucas.Providers.Management.IntegrationTests\bin\Release\net*)[0].FullName)\*" `
        .\Ucas.Providers.Management.IntegrationTests\Publish\ProvidersManagementIntegrationTests -Recurse

    # Source from Ucas.Providers.Management.Api:
    # binaries from System Under Test (Ucas.Providers.Management.Api) go into the Ucas.Providers.Management.IntegrationTests bin folder,
    # but content files (e.g. stuff in wwwroot) do not - instead the tests look for them at runtime in the original source directory;
    # since we don't know exactly what we'll need, we'll just take all of it (minus the bin and obj folders, to save space)
    Copy-Item .\Ucas.Providers.Management.Api .\Ucas.Providers.Management.IntegrationTests\Publish\Ucas.Providers.Management.Api -Recurse
    Remove-Item -Force .\Ucas.Providers.Management.IntegrationTests\Publish\Ucas.Providers.Management.Api\bin -Recurse
    Remove-Item -Force .\Ucas.Providers.Management.IntegrationTests\Publish\Ucas.Providers.Management.Api\obj -Recurse

    # Solution file:
    # the default mechanism for locating SUT content files discussed above
    # involves going up the directory tree looking for a .sln file, so we need to keep ours
    Copy-Item .\Ucas.Providers.Management.sln .\Ucas.Providers.Management.IntegrationTests\Publish
    Write-Host "+----------------------------------------------------------------------------------------------"
}

$exitCode = 0

try {
    Write-Host "+----------------------------------------------------------------------------------------------"
    Write-Host "| STARTING Build"
    Write-Host "+----------------------------------------------------------------------------------------------"

    Write-Parameters

    ToolsInfo

    $Projects = DiscoverProjects

    Clean
    RestoreAndBuild
    UnitTest
    PublishProvidersManagementApiTests
    PublishMessageConsumerIntegrationTests
    PublishProvidersManagementIntegrationTests
    PublishDbMigrator
    PublishMainLambda
    PublishMessageConsumerLambda

    Write-Host "+---------------------------------------------------------------------------------------------"
    Write-Host "| SUCCEEDED"
    Write-Host "+----------------------------------------------------------------------------------------------"
}
catch [Exception] {
    $exitCode = 1
    Write-Host "+------------------------- EXCEPTION CAUGHT ------------------------- "
    Write-Error $_.Exception -ErrorAction Continue | Format-List -Force
    Write-Host "+-------------------------------------------------------------------- "
}
finally {
    Write-Host "| Exited with code [$exitCode]"
    Write-Host "+-------------------------------------------------------------------- "
    exit $exitCode
}