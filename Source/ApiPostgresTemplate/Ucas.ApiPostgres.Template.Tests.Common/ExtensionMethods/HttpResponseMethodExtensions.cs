﻿using Newtonsoft.Json;
using System.Net.Http;
using System.Threading.Tasks;

namespace Ucas.ApiPostgres.Template.Tests.Helpers
{
    public static class HttpResponseMethodExtensions
    {
        public static async Task<T> ConvertTo<T>(this HttpResponseMessage httpResponse) =>
            JsonConvert.DeserializeObject<T>(await httpResponse.Content.ReadAsStringAsync());
    }
}
