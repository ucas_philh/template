﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ucas.ApiPostgres.Template.Db.Context;
using Ucas.ApiPostgres.Template.Db.Entities;

namespace Ucas.ApiPostgres.Template.Tests.Helpers
{
    public static class DataSeedHelper
    {

        /// <summary>
        /// Seed some dummy data into your test database context
        /// </summary>
        /// <param name="context">The context to add the data into</param>
        public static IEnumerable<object> SeedTestData(ApplicationContext context)
        {


            var entitiesToSave = context.ChangeTracker.Entries().Select(e => e.Entity).ToList();

            context.SaveChanges();

            return entitiesToSave;
        }
    }
}
