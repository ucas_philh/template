using System;
using System.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Serilog;
using Ucas.ApiPostgres.Template.Core.Helpers;

namespace Ucas.ApiPostgres.Template.Api
{
    public class Program
    {
        public static int Main(string[] args)
        {
            // Use W3C Trace Context format for correlation ids in HTTP headers and logs
            // https://devblogs.microsoft.com/aspnet/improvements-in-net-core-3-0-for-troubleshooting-and-monitoring-distributed-apps/
            Activity.DefaultIdFormat = ActivityIdFormat.W3C;

            //Setup logging outside of Host configuration to catch startup errors
            Logging.ConfigureSerilog();

            try
            {
                Log.Information("Starting web host [{0}]", Process.GetCurrentProcess().ProcessName);
                CreateHostBuilder(args).Build().Run();
                return 0;
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "Host terminated unexpectedly");
                return 1;
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                })
                .UseSerilog();
    }
}
