﻿using System;
using Microsoft.AspNetCore.Mvc.Filters;
using Serilog.Events;
using Serilog;

namespace Ucas.ApiPostgres.Template.Api.Filters
{
    /// <summary>
    /// Add MVC-specific data to Serilog's per-request log entry
    /// </summary>
    public class SerilogMvcLoggingFilter : IActionFilter
    {
        private readonly IDiagnosticContext _diagnosticContext;
        public SerilogMvcLoggingFilter(IDiagnosticContext diagnosticContext)
        {
            _diagnosticContext = diagnosticContext;
        }

        void AddMvcProperties(FilterContext context)
        {
            _diagnosticContext.Set("MvcRouteData", context.ActionDescriptor.RouteValues);
            _diagnosticContext.Set("MvcActionName", context.ActionDescriptor.DisplayName);
            _diagnosticContext.Set("MvcActionId", context.ActionDescriptor.Id);
            _diagnosticContext.Set("MvcModelStateIsValid", context.ModelState.IsValid);
        }

        public void OnActionExecuting(ActionExecutingContext context) => AddMvcProperties(context);

        public void OnActionExecuted(ActionExecutedContext context) { }

        public void OnPageHandlerExecuted(PageHandlerExecutedContext context) { }

        public void OnPageHandlerExecuting(PageHandlerExecutingContext context) => AddMvcProperties(context);

        public void OnPageHandlerSelected(PageHandlerSelectedContext context) { }
    }

    /// <summary>
    /// Specifies the level at which to log summary information about successful requests.
    /// Only needed if a level other than the default - <see cref="LogEventLevel.Information"/> - is required.
    /// Failed requests (those that throw an exception or result in a 5xx status code) are always logged at <see cref="LogEventLevel.Error"/> level.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class RequestLogLevelAttribute : Attribute
    {
        public LogEventLevel Level { get; set; } = LogEventLevel.Information;
    }
}
