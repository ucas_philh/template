using Amazon.Extensions.Configuration.SystemsManager;
using Amazon.Lambda.APIGatewayEvents;
using Amazon.Lambda.AspNetCoreServer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Npgsql;
using Serilog;
using Serilog.AspNetCore;
using Serilog.Events;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Security.Claims;
using System.Text.RegularExpressions;
using Ucas.ApiPostgres.Template.Api.Filters;
using Ucas.ApiPostgres.Template.Core.Providers;
#if (AutoMapper)
using Ucas.ApiPostgres.Template.Core.MappingProfiles;
#endif
#if (UseFluentValidation)
using Ucas.ApiPostgres.Template.Core.Validation;
#endif
using Ucas.ApiPostgres.Template.Db;
using Ucas.ApiPostgres.Template.Db.Context;
#if (UseOutboxClient)
using Ucas.Message.Exchange.OutboxClient;
using Ucas.Message.Exchange.OutboxClient.Postgres;
using Ucas.Message.Exchange.OutboxPublisher.Db;
#endif

namespace Ucas.ApiPostgres.Template.Api
{
    public class Startup
    {
        private readonly IConfiguration _configuration;

        private static bool IsCloudDevelopmentEnvironment =>
        Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "TeamEnvironment";

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddHealthChecks();

            services.AddDbContext<Db.Context.ApplicationContext>(options =>
                options.UseNpgsql(DatabaseConnectionHelper.GetConnectionString(_configuration))
                    .UseSnakeCaseNamingConvention().EnableDetailedErrors());

            services.AddScoped<IDateTimeProvider, DateTimeProvider>();

#if (UseFluentValidation)
            services.AddScoped<IValidator, Validator>();
#endif
#if (AutoMapper)
            services.AddAutoMapper(typeof(DtoProfileMapper));
#endif
            services.AddControllers(opts =>
            {
                opts.Filters.Add<SerilogMvcLoggingFilter>();
            })
            .AddNewtonsoftJson(options =>
            {
                options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
            })
            .ConfigureApiBehaviorOptions(opts =>
            {
                // any error values not mapped here will be translated as error 500
                opts.ClientErrorMapping[400] = new ClientErrorData
                {
                    Title = "Bad request",
                    Link = "https://httpstatuses.com/400"
                };
                opts.ClientErrorMapping[401] = new ClientErrorData
                {
                    Title = "Unauthorized",
                    Link = "https://httpstatuses.com/401"
                };
                opts.ClientErrorMapping[403] = new ClientErrorData
                {
                    Title = "Forbidden",
                    Link = "https://httpstatuses.com/403"
                };
                opts.ClientErrorMapping[404] = new ClientErrorData
                {
                    Title = "Not found",
                    Link = "https://httpstatuses.com/404"
                };
                opts.ClientErrorMapping[500] = new ClientErrorData
                {
                    Title = "Internal server error",
                    Link = "https://httpstatuses.com/500"
                };
                opts.ClientErrorMapping[502] = new ClientErrorData
                {
                    Title = "Bad gateway",
                    Link = "https://httpstatuses.com/502"
                };
            });

            // Register the Swagger generator, defining 1 or more Swagger documents
            services.AddSwaggerGen(opts =>
            {
                opts.SwaggerDoc("v1", new OpenApiInfo { Title = "SWAGGER-TITLE", Version = "v1" });

                // Set the comments path for the Swagger JSON and UI.
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(System.AppContext.BaseDirectory, xmlFile);
                opts.IncludeXmlComments(xmlPath);
            });
#if (UseOutboxClient)
            services.Configure<DatabaseOptions>(_configuration.GetSection("OutboxDatabase"));
            services.UsePostgresOutboxClient();
            services.AddOutboxClient();

            services.AddScoped<IMessageRegistryRepository, PostgresMessageRegistryRepository>();
#endif
            services.AddMemoryCache();
            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                CreateAndMigrateDatabase();
            }
            else
            {
                app.Use(async (_, next) =>
                {
                    _configuration.WaitForSystemsManagerReloadToComplete(TimeSpan.FromSeconds(5));
                    await next();
                });
            }

            // Setup Serilog to write a log entry for each incoming Request
            // It's important that this is kept before other middleware, so that it wraps everything else
            app.UseSerilogRequestLogging(ConfigureRequestLogging);

            app.UseHttpsRedirection();

            app.UseRouting();

            if (env.IsDevelopment() || IsCloudDevelopmentEnvironment)
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHttpsRedirection();
            }

            //Enable middleware to serve generated Swagger as a JSON endpoint.
            var urlSufix = (Environment.GetEnvironmentVariable("GatewayPrefix") ?? "") == "" ?
                             "" : Environment.GetEnvironmentVariable("GatewayPrefix");

            app.UseSwagger(c =>
            {
                c.PreSerializeFilters.Add((swaggerDoc, httpReq) =>
                {
                    swaggerDoc.Servers = new List<OpenApiServer>
                    {
                        new OpenApiServer
                        {
                            Url = $"{httpReq.Scheme}://{httpReq.Host.Value}{urlSufix}"
                        }  
                    };
                });
            });

            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("./swagger/v1/swagger.json", "SWAGGER-TITLE");
                c.RoutePrefix = string.Empty;
            });

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints
                    .MapHealthChecks("api/healthcheck")
                    .WithMetadata(
                        new RequestLogLevelAttribute { Level = LogEventLevel.Debug }
                    );
            });
        }

        private void CreateAndMigrateDatabase()
        {
            var postgres =
                new NpgsqlConnectionStringBuilder(
                    DatabaseHelpers.GetUnprocessedConnectionString(_configuration, "db_Migrator"))
                {
                    Database = "postgres"
                };
            var apiUser =
                new NpgsqlConnectionStringBuilder(
                    DatabaseHelpers.GetUnprocessedConnectionString(_configuration, "db"));
            var readOnlyUser =
                new NpgsqlConnectionStringBuilder(
                    DatabaseHelpers.GetUnprocessedConnectionString(_configuration, "db_RO"));
            var migrator =
                new NpgsqlConnectionStringBuilder(
                    DatabaseHelpers.GetUnprocessedConnectionString(_configuration, "db_Migrator"));

            var dbContext = new ApplicationContext(new DbContextOptionsBuilder<ApplicationContext>().UseNpgsql(migrator.ConnectionString)
                .UseSnakeCaseNamingConvention().Options);

            DatabaseHelpers.CreateApplicationDatabaseAndUser(apiUser, readOnlyUser, migrator, postgres.ConnectionString);

            dbContext.Database.Migrate();
        }
        private static void ConfigureRequestLogging(RequestLoggingOptions opts)
        {
            opts.EnrichDiagnosticContext = (logContext, httpContext) =>
            {
                // Serilog will log RequestMethod, RequestPath, StatusCode and Elapsed automatically
                logContext.Set("RequestHost", httpContext.Request.Host);
                logContext.Set("RequestProtocol", httpContext.Request.Protocol);
                logContext.Set("RequestScheme", httpContext.Request.Scheme);
                logContext.Set("RequestSourceIp", httpContext.Connection.RemoteIpAddress);
                logContext.Set("RequestForwardedFor", httpContext.Request.Headers["X-Forwarded-For"]);
                logContext.Set("RequestForwardedHost", httpContext.Request.Headers["X-Forwarded-Host"]);
                logContext.Set("RequestForwardedProtocol", httpContext.Request.Headers["X-Forwarded-Proto"]);
                if (httpContext.Request.QueryString.HasValue)
                    logContext.Set("RequestQueryString", httpContext.Request.QueryString.Value);
                if (httpContext.Items.TryGetValue(AbstractAspNetCoreFunction.LAMBDA_REQUEST_OBJECT, out var value) &&
                    value is APIGatewayProxyRequest proxyRequest)
                {
                    logContext.Set("ElapsedAtGateway", DateTimeOffset.Now.ToUnixTimeMilliseconds() - proxyRequest.RequestContext.RequestTimeEpoch);
                }
                if (httpContext.GetEndpoint() is { } endpoint)
                    logContext.Set("EndpointName", endpoint.DisplayName);
                if (httpContext.User != null)
                {
                    void SetIfClaimExists(string claimType, string propertyName)
                    {
                        var claim = httpContext.User.FindFirst(claimType);
                        if (claim != null)
                            logContext.Set(propertyName, claim.Value);
                    }
                    SetIfClaimExists("Ucas_AccountId", "UserUcasAccountId");
                    SetIfClaimExists("Ucas_SystemId", "UserSystemId");
                    SetIfClaimExists("Ucas_SubSystemId", "UserSubSystemId");
                    SetIfClaimExists("Ucas_SystemName", "UserSystemName");
                    // NameIdentifier will either be:
                    // a GigyaId for individual users,
                    // an empty guid for Systems not proxying a user request,
                    // or {SystemId}--{UserId} for Systems proxying a user request
                    var nameIdClaimValue = httpContext.User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
                    if (nameIdClaimValue != null)
                    {
                        if (httpContext.User.HasClaim("Ucas_JwtType", "SystemIdentity")) // System
                        {
                            var match = Regex.Match(nameIdClaimValue, "^.*--(?<id>.*)$");
                            if (match.Success)
                            {
                                logContext.Set("UserId", match.Groups["id"].Value);
                            }
                        }
                        else // Individual user
                        {
                            logContext.Set("UserId", nameIdClaimValue);
                        }
                    }
                }
            };
            // Default behaviour is to log every request,
            // but we don't really need that for monitoring endpoints, like the health check.
            // So - provide an override mechanism via RequestLogLevelAttribute.
            opts.GetLevel = (context, _, exception) =>
            {
                if (exception != null || context.Response.StatusCode > 499)
                    return LogEventLevel.Error;
                if (context.GetEndpoint()?.Metadata.GetMetadata<RequestLogLevelAttribute>() is { } overrideAttribute)
                    return overrideAttribute.Level;
                return LogEventLevel.Information;
            };
        }
    }
}
