﻿#if (AutoMapper)
using AutoMapper;
#endif
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;
using Ucas.ApiPostgres.Template.Core.Problems;
using Ucas.ApiPostgres.Template.Core.Services;
using Ucas.ApiPostgres.Template.Db.Entities;
using JsonSerializer = System.Text.Json.JsonSerializer;


namespace Ucas.ApiPostgres.Template.Api.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [SuppressMessage("ReSharper", "StringLiteralTypo")]
    [ApiController]
    [AllowAnonymous]
    [Route("api/changeme")]
    public class EmptyController : UcasControllerBase
    {
#if (AutoMapper)
        private readonly IMapper _mapper;
#endif
#if (AutoMapper)
        public EmptyController(IMapper mapper, IDiagnosticContext diagnosticContext) : base(diagnosticContext)
        {
            _mapper = mapper;
        }
#else 
        public EmptyController(IDiagnosticContext diagnosticContext) : base(diagnosticContext)
        {
        }
#endif
    }
}
