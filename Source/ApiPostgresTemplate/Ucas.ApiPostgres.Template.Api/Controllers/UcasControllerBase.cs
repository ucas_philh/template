﻿using Microsoft.AspNetCore.Mvc;
using Serilog;
using Ucas.ApiPostgres.Template.Core.Problems;

namespace Ucas.ApiPostgres.Template.Api.Controllers
{
    public class UcasControllerBase : ControllerBase
    {
        protected readonly IDiagnosticContext _diagnosticContext;

        public UcasControllerBase(IDiagnosticContext diagnosticContext)
        {
            _diagnosticContext = diagnosticContext;
        }

        protected ObjectResult Problem(string type, params string[] additionalInformation)
        {
            return Problem(type, ProblemHelper.GetDetail(type, additionalInformation));
        }

        protected ObjectResult Problem(string type, object? additionalInformation)
        {
            return (additionalInformation == null)
                ? Problem(type, ProblemHelper.GetDetail(type))
                : Problem(type, ProblemHelper.GetDetail(additionalInformation));
        }

        private ObjectResult Problem(string type, string detail)
        {
            _diagnosticContext.Set("ProblemDetail", detail);

            int statusCode = ProblemHelper.GetStatusCode(type);
            _diagnosticContext.Set("ProblemStatusCode", statusCode);

            string title = ProblemHelper.GetTitle(type);
            _diagnosticContext.Set("ProblemTitle", title);

            _diagnosticContext.Set("ProblemType", type);

            return Problem(
                detail: detail,
                instance: null,
                statusCode: statusCode,
                title: title,
                type);
        }
    }
}
