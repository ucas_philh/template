﻿using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Serilog;
using System;
using System.Threading.Tasks;

namespace Ucas.ApiPostgres.Template.Api.ExtensionMethods
{
    public static class IMemoryCacheExtensions
    {
        public static async Task<T> GetValueWithStrategyAsync<T>(
            this IMemoryCache cache,
            object key,
            Func<Task<T>> strategy,
            IConfiguration configuration,
            IDiagnosticContext? logContext = null)
        {
            if (cache.TryGetValue(key, out T value))
            {
                logContext?.Set("Cache used", true);
            }
            else
            {
                logContext?.Set("Cache used", false);

                value = await strategy();

                var expirationInMinutes = double.Parse(configuration["Cache:ExpirationInMinutes"]);

                cache.Set(key, value, new MemoryCacheEntryOptions { SlidingExpiration = TimeSpan.FromMinutes(expirationInMinutes) });
            }

            return value;
        }
    }
}
