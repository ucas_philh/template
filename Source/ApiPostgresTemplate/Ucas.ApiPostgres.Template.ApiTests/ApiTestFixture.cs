﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using System;
using System.IO;
using System.Threading.Tasks;
using Ucas.ApiPostgres.Template.Api;
using Ucas.ApiPostgres.Template.Db;
using Ucas.ApiPostgres.Template.Db.Context;
using Xunit;

namespace Ucas.ApiPostgres.Template.ApiTests
{
    [CollectionDefinition(ApiTestFixture.CollectionName)]
    public class ApiTestCollection : ICollectionFixture<ApiTestFixture>
    {
        // This class has no code, and is never created. Its purpose is simply
        // to be the place to apply [CollectionDefinition] and all the
        // ICollectionFixture<> interfaces.

        // https://xunit.net/docs/shared-context#collection-fixture
    }

    public class ApiTestFixture : IAsyncLifetime
    {
        public const string CollectionName = "API Tests";

        private bool _isRunningInBamboo;
        private string _apiUrl;
        private IHost _host;

        public IConfiguration Config { get; private set; }
        public Db.Context.ApplicationContext DbContext { get; private set; }
       
        public Uri GenerateUri(string path)
        {
            if (path.StartsWith("/"))
                path = path.Substring(1);

            return new Uri($"{_apiUrl}/{path}");
        }

        public ApiTestFixture()
        {
            var config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory());

            // check if we are running these tests in bamboo or locally to load the right config file
            if (bool.TryParse(Environment.GetEnvironmentVariable("API_TEST_IN_BAMBOO"), out _isRunningInBamboo) && _isRunningInBamboo)
            {
                config.AddJsonFile("appsettings.Bamboo.json");
            }
            else
            {
                Environment.SetEnvironmentVariable("ASPNETCORE_ENVIRONMENT", "Development");
                config.AddJsonFile("appsettings.local.json");
                config.AddEnvironmentVariables();
            }

            Config = config.Build();

            _apiUrl = Config["ServiceUrls:Service"];

            DbContext = new ApplicationContext(new DbContextOptionsBuilder<ApplicationContext>()
                .UseNpgsql(DatabaseConnectionHelper.GetConnectionString(Config))
                .UseSnakeCaseNamingConvention()
                .Options);
        }

        public async Task InitializeAsync()
        {
            if (!_isRunningInBamboo)
            {
                // start the API on the port that the tests will use
                Environment.SetEnvironmentVariable("ASPNETCORE_URLS", _apiUrl);
                _host = Program.CreateHostBuilder(new string[] { }).Build();
                await _host.StartAsync();
            }
        }

        public async Task DisposeAsync()
        {
            await DbContext.DisposeAsync();

            if (!_isRunningInBamboo)
            {
                await _host.StopAsync();
                // we need to dispose otherwise the test won't cleanly exit.
                _host.Dispose();
            }
        }
    }
}
