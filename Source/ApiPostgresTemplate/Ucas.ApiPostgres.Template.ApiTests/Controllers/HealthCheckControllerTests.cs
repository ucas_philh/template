﻿using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace Ucas.ApiPostgres.Template.ApiTests.Controllers
{
    [Collection(ApiTestFixture.CollectionName)]
    public class HealthCheckControllerTests
    {
        private readonly ApiTestFixture _fixture;

        public HealthCheckControllerTests(ApiTestFixture fixture) =>
            _fixture = fixture;

        [Fact]
        public async Task GetReturns200()
        {
            var uri = _fixture.GenerateUri("/api/healthcheck");

            using var client = new HttpClient();

            var response = await client.GetAsync(uri);
            response.EnsureSuccessStatusCode();
        }
    }
}
