using FluentValidation;
using FluentValidation.Results;
using System.Linq;
using Ucas.ApiPostgres.Template.Db.Entities;
using ValidationErrors = System.Collections.Generic.Dictionary<string, System.Collections.Generic.IEnumerable<Ucas.ApiPostgres.Template.Core.Validation.ValidationError>>;

namespace Ucas.ApiPostgres.Template.Core.Validation
{
    public class Validator : IValidator
    {
        public static readonly string RefDataKey = "refData";        

        public bool Validate<T>(T target, AbstractValidator<T> validator, out ValidationErrors validationErrors)
        {
            var context = new ValidationContext<T>(target)
            {
                
            };

            return IsResultValid(validator.Validate(context), out validationErrors);
        }

        public bool IsResultValid(ValidationResult result, out ValidationErrors validationErrors)
        {
            validationErrors = new ValidationErrors();

            if (result.IsValid) return true;

            validationErrors = result.Errors.GroupBy(e => e.PropertyName).ToDictionary(g => g.Key, g => g.Select(ToValidationError));

            return false;
        }

        private ValidationError ToValidationError(ValidationFailure validationFailure) =>
            new ValidationError(
                (validationFailure.CustomState as CustomValidationState)?.UcasErrorCode ?? ValidationErrorCodes.Unspecified,
                validationFailure.ErrorMessage);
    }
}
