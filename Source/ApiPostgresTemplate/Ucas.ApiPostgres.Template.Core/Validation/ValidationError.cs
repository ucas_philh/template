namespace Ucas.ApiPostgres.Template.Core.Validation
{
    public class ValidationError
    {
        /// <summary>
        /// A locale-independent identifier of the error type: this forms our contract with consumers
        /// </summary>
        public int Code { get; }

        /// <summary>
        /// A (potentially) localised description of the error
        /// </summary>
        public string Message { get; }

        public ValidationError(int code, string message)
        {
            Code    = code;
            Message = message;
        }
    }
}