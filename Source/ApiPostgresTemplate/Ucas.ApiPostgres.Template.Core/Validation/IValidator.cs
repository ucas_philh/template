﻿using FluentValidation;
using FluentValidation.Results;
using System.Collections.Generic;
using Ucas.ApiPostgres.Template.Db.Entities;

namespace Ucas.ApiPostgres.Template.Core.Validation
{
    public interface IValidator
    {
        bool Validate<T>(T target, AbstractValidator<T> validator,
            out Dictionary<string, IEnumerable<ValidationError>> validationErrors);

        bool IsResultValid(ValidationResult result, out Dictionary<string, IEnumerable<ValidationError>> validationErrors);
    }
}