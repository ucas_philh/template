namespace Ucas.ApiPostgres.Template.Core.Validation
{
    public static class ValidationErrorCodes
    {
        public static readonly int Unspecified            = -1;
        public static readonly int NullOrEmpty            = 1;
    }
}