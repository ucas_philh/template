﻿using Serilog;
using Serilog.Core;
using Serilog.Events;
using Serilog.Formatting.Compact;
using System;
using System.Diagnostics;
using System.Reflection;

namespace Ucas.ApiPostgres.Template.Core.Helpers
{
    public static class Logging
    {
        /// <summary>
        /// Remove SQL command text in order to keep down log file size
        /// </summary>
        private class RemoveSqlEnricher : ILogEventEnricher
        {
            public void Enrich(LogEvent logEvent, ILogEventPropertyFactory propertyFactory)
            {
                if (logEvent.Properties.TryGetValue("SourceContext", out var sourceContext))
                {
                    if (sourceContext.ToString() == "\"Microsoft.EntityFrameworkCore.Database.Command\"")
                    {
                        logEvent.RemovePropertyIfPresent("commandText");
                    }
                }
            }
        }

        public static void ConfigureSerilog()
        {
            var devEnvironment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Development";

            var logConfig = new LoggerConfiguration();

            // 1. Log-levels
            //    Allow a global override via environment variable
            if (Environment.GetEnvironmentVariable("LOG_LEVEL_OVERRIDE") is { } logLevel &&
                Enum.TryParse(logLevel, true, out LogEventLevel level))
                logConfig = logConfig.MinimumLevel.Is(level);
            else
                //    Otherwise, set things up such that we get:
                logConfig = logConfig
                    .MinimumLevel.Information() // 1 log entry per incoming Request (via Serilog's RequestLogging middleware)
                    .MinimumLevel.Override("Microsoft", LogEventLevel.Warning) // Framework or system-level errors
                    .MinimumLevel.Override("System", LogEventLevel.Warning)
                    .MinimumLevel.Override("Microsoft.EntityFrameworkCore.Database.Command", LogEventLevel.Information) // 1 log entry per EF db query
                    .MinimumLevel.Override("System.Net.Http.HttpClient", LogEventLevel.Information) // 1 log entry per outgoing HttpClient Request
                    .Filter.ByExcluding(
                        "StartsWith(SourceContext, 'System.Net.Http.HttpClient') and not (EventId.Id = 101 and EndsWith(SourceContext, 'ClientHandler'))")
                    .Enrich.With<RemoveSqlEnricher>();

            // 2. Outputs
            logConfig = logConfig
            // Summaries to Console
                .WriteTo.Console(new RenderedCompactJsonFormatter());
            // Summaries to Debug when in dev environment
            if (devEnvironment)
            {
                logConfig = logConfig
                    .WriteTo.Debug()
                    .WriteTo.File(
                        new RenderedCompactJsonFormatter(),
                        $"C:\\logs\\{Assembly.GetExecutingAssembly().GetName().Name}.log",
                        fileSizeLimitBytes: 10 * 1024 * 1024, // 10MB in dev
                        shared: true,
                        flushToDiskInterval: TimeSpan.FromSeconds(2),
                        rollOnFileSizeLimit: true,
                        retainedFileCountLimit: 15);
            }

            Log.Logger = logConfig
                .Enrich.FromLogContext()
                .Enrich.With<CurrentActivityEnricher>()
                .CreateLogger();
        }

        // Add tracing ids to all Serilog log events,
        // Previously these were added automatically when adding Activity.DefaultIdFormat = ActivityIdFormat.W3C;
        // This changed in .net5 https://github.com/serilog/serilog-aspnetcore/issues/207 so we need to manually add them.
        public class CurrentActivityEnricher : ILogEventEnricher
        {
            public void Enrich(LogEvent logEvent, ILogEventPropertyFactory propertyFactory)
            {
                var activity = Activity.Current;
                if (activity == null) return;

                logEvent.AddPropertyIfAbsent(new LogEventProperty("TraceId",
                    new ScalarValue(activity.TraceId.ToHexString())));
                logEvent.AddPropertyIfAbsent(new LogEventProperty("SpanId",
                    new ScalarValue(activity.SpanId.ToHexString())));
                logEvent.AddPropertyIfAbsent(new LogEventProperty("ParentId",
                    new ScalarValue(activity.ParentSpanId.ToHexString())));
            }
        }
    }
}
