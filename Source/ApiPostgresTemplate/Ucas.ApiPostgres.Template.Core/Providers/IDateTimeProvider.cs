﻿using System;

namespace Ucas.ApiPostgres.Template.Core.Providers
{
    public interface IDateTimeProvider
    {
        DateTimeOffset UtcNow { get; }
    }
}
