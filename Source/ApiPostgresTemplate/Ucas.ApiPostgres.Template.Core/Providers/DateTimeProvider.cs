﻿using System;

namespace Ucas.ApiPostgres.Template.Core.Providers
{
    public class DateTimeProvider : IDateTimeProvider
    {
        public DateTimeOffset UtcNow => DateTimeOffset.UtcNow;
    }
}
