using System;
using System.Linq;
using Force.DeepCloner;

namespace Ucas.ApiPostgres.Template.Core.Extensions
{
    public static class ObjectExtensions
    {
        public static T With<T>(this T x, Action<T> f)
        {
            var copy = x.DeepClone();
            f(copy);
            return copy;
        }

    }
}