using FluentValidation;
using Ucas.ApiPostgres.Template.Core.Validation;

namespace Ucas.ApiPostgres.Template.Core.Extensions
{
    public static class ValidationExtensions
    {
        public static IRuleBuilderOptions<T, TProp> WithUcasErrorCode<T, TProp>(this IRuleBuilderOptions<T, TProp> ruleBuilderOptions, int code) =>
            ruleBuilderOptions.WithState(x => new CustomValidationState { UcasErrorCode = code });

        public static IRuleBuilderOptions<T, TProp> NotEmptyWithErrorCode<T, TProp>(this IRuleBuilder<T, TProp> ruleBuilder) =>
            ruleBuilder.NotEmpty().WithUcasErrorCode(ValidationErrorCodes.NullOrEmpty);
    }
}