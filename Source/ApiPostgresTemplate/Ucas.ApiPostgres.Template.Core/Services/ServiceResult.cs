﻿using System.Collections.Generic;
#if (UseFluentValidation)
using Ucas.ApiPostgres.Template.Core.Validation;
#endif

namespace Ucas.ApiPostgres.Template.Core.Services
{
    public class ServiceResult<T> : ServiceResult
    {
        public ServiceResult() : base()
        {
            Result = default(T)!;
        }

        public ServiceResult(T result, bool isSuccess) : base(isSuccess)
        {
            Result = result;
        }

        public T Result { get; set; }
    }
    public class ServiceResult
    {
        public ServiceResult()
        {
            IsSuccess = false;
        }

        public ServiceResult(bool isSuccess)
        {
            IsSuccess = isSuccess;
        }

        public bool IsSuccess { get; set; }
        public string ProblemType { get; set; } = string.Empty;
        public List<string> AdditionalInformation { get; set; } = new List<string>();
#if (UseFluentValidation)
        public Dictionary<string, IEnumerable<ValidationError>>? Errors { get; set; }
#endif
    }
}