﻿namespace Ucas.ApiPostgres.Template.Core.Problems
{
    /// <summary>
    /// Types to use in ProblemDetails objects.
    /// </summary>
    /// <remarks>
    /// Any new type added here should also be added to the TitleDictionary and DetailDictionary in ProblemHelper
    /// </remarks>
    public static class ProblemTypes
    {
        /// <summary>
        /// One or more validation errors occurred
        /// </summary>
        public const string ValidationError = "validation-error";

        // NotFound types
        public const string ResourceNotFound = "resource-not-found";

        // Save answer error
        public const string SaveAnswerError = "save-answer-error";

        // Read answer error
        public const string ReadAnswerError = "read-answer-error";

        // Read answer error
        public const string ReadReferenceDataError = "read-reference-data-error";

    }
}
