﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Ucas.ApiPostgres.Template.Core.Problems
{
    public static class ProblemHelper
    {
        /// <summary>
        /// Problem type linked to title
        /// </summary>
        /// <remarks>
        /// Title should be "a short, human-readable summary of the problem type. It SHOULD NOT change from occurrence to occurrence of the problem".
        /// </remarks>
        private static readonly Dictionary<string, string> TitleDictionary = new Dictionary<string, string>
        {
            { ProblemTypes.ValidationError, "Validation error" },
            { ProblemTypes.ResourceNotFound, "Resource not found" },
        };

        /// <summary>
        /// Problem type linked to detail
        /// </summary>
        /// <remarks>
        /// Detail should be "a human-readable explanation specific to this occurrence of the problem".
        /// </remarks>
        private static readonly Dictionary<string, string> DetailDictionary = new Dictionary<string, string>
        {
            { ProblemTypes.ValidationError, "Validation error: %" },
            { ProblemTypes.ResourceNotFound, "Resource not found: %" },
        };

        /// <summary>
        /// Problem types which should result in a NotFound status code
        /// </summary>
        private static readonly List<string> NotFoundProblemTypes = new List<string>()
        {
            ProblemTypes.ResourceNotFound
        };

        /// <summary>
        /// Problem types which should result in a InternalServerError status code
        /// </summary>
        private static readonly List<string> InternalServerErrorProblemTypes = new List<string>()
        {

        };

        public static string GetTitle(string type)
        {
            return TitleDictionary[type];
        }

        public static string GetDetail(string type, params string[] additionalInformation)
        {
            string detail = DetailDictionary[type];

            var regex = new Regex(Regex.Escape("%"));
            foreach (var additionalInformationItem in additionalInformation)
            {
                string item = additionalInformationItem ?? "";
                detail = regex.Replace(detail, item, 1);
            }

            return detail;
        }

        public static string GetDetail(object? additionalInformation)
        {
            string detail = additionalInformation != null ? JsonConvert.SerializeObject(additionalInformation) : string.Empty;
            return detail;
        }

        public static int GetStatusCode(string type)
        {
            if (NotFoundProblemTypes.Contains(type))
                return StatusCodes.Status404NotFound;

            if (InternalServerErrorProblemTypes.Contains(type))
            {
                return StatusCodes.Status500InternalServerError;
            }

            return StatusCodes.Status400BadRequest;
        }
    }
}
