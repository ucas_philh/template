﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using System;
using System.Threading.Tasks;
using Ucas.ApiPostgres.Template.Db.Context;

namespace Ucas.ApiPostgres.Template.UnitTests
{
    public class EfFixture : IDisposable
    {
        public Db.Context.ApplicationContext DataContext;
        private IServiceProvider _serviceProvider;
        public string EfGuid = Guid.NewGuid().ToString();

        public EfFixture()
        {
            var serviceProvider = new Mock<IServiceProvider>();
            var options = new DbContextOptionsBuilder<Db.Context.ApplicationContext>()
                .UseInMemoryDatabase(EfGuid).Options;
            serviceProvider.Setup(x => x.GetService(typeof(Db.Context.ApplicationContext))).Returns(new Db.Context.ApplicationContext(options: options));

            var serviceScope = new Mock<IServiceScope>();
            serviceScope.Setup(x => x.ServiceProvider).Returns(serviceProvider.Object);

            var serviceScopeFactory = new Mock<IServiceScopeFactory>();
            serviceScopeFactory.Setup(x => x.CreateScope()).Returns(serviceScope.Object);

            serviceProvider.Setup(x => x.GetService(typeof(IServiceScopeFactory))).Returns(serviceScopeFactory.Object);

            _serviceProvider = serviceProvider.Object;
        }

        public void Initialize()
        {
            var options = new DbContextOptionsBuilder<Db.Context.ApplicationContext>()
                .UseInMemoryDatabase(EfGuid).Options;

            DataContext = new Db.Context.ApplicationContext(options);

            SeedDatabase();
        }

        private void SeedDatabase()
        {
 
        }

        public void Dispose()
        {
            DataContext.Database.EnsureDeleted();
            DataContext?.Dispose();
        }
        public async Task DisposeAsync()
        {
            await DataContext.Database.EnsureDeletedAsync();
            DataContext?.Dispose();
        }
    }
}
