﻿using Microsoft.EntityFrameworkCore.Design;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using Ucas.ApiPostgres.Template.Db.Context;

namespace Ucas.ApiPostgres.Template.Db.MigratorApp
{
    class Program : IDesignTimeDbContextFactory<ApplicationContext>
    {
        public ApplicationContext CreateDbContext(string[] args)
        {
            // When running "dotnet ef migrations add migrationName" to create the migration it will run CreateDbContext
            // but it won't pass any arguments. So use a placeholder string for connection name as we don't need it for creating the migration scripts
            // as we do this locally.
            var connectionString = args.Length != 0 ? args[0] : "not-used";

            var optionsBuilder = new DbContextOptionsBuilder<ApplicationContext>()
                .UseNpgsql(connectionString)
                .UseSnakeCaseNamingConvention();

            return new ApplicationContext(optionsBuilder.Options);
        }

        // AWS credentials are passed to the task as environment variables in the bamboo task.
        // The AWS SDK automatically looks for these variables so we don't need to make reference to them.
        static int Main(string[] args)
        {
            if (args.Length != 2)
            {
                Console.WriteLine("You did not provide sufficient command line arguments. Exiting");
                Console.WriteLine("Usage: Ucas.Providers.Management.Db.Migrations.exe \"environmentPrefix\" \"DatabaseName\"");
                Console.WriteLine("environmentPrefix is the prefix applied to Terraform resources");
                Console.WriteLine("DatabaseName is the name of the database to create and/or migrate");
                return 1;
            }

            var environmentPrefix = args[0];
            var databaseName = args[1];

            Console.WriteLine("----------------------------------------------------");
            Console.WriteLine("Attempting to obtain connection details from Secrets Manager");
            Console.WriteLine("----------------------------------------------------");

            // Obtain the secrets. when using Secrets Manager secrets you must always add "/aws/reference/secretsmanager/" before the name of 
            // the secret you wish to obtain.
            var builder = new ConfigurationBuilder()
                .AddSystemsManager(configureSource =>
                {
                    configureSource.Path = $"/aws/reference/secretsmanager/{environmentPrefix}-ProvidersManagementRDSRootUserCredentials";
                })
                .AddSystemsManager(configureSource =>
                {
                    configureSource.Path = $"/aws/reference/secretsmanager/{environmentPrefix}-ProvidersManagementRDSApiUserCredentials";
                })
                .AddSystemsManager(configureSource =>
                {
                    configureSource.Path = $"/aws/reference/secretsmanager/{environmentPrefix}-ProvidersManagementRDSReadOnlyUserCredentials";
                })
                .AddCommandLine(args);

            var configuration = builder.Build();

            Console.WriteLine("----------------------------------------------------");
            Console.WriteLine("Starting Creation of database and users");
            Console.WriteLine("----------------------------------------------------");

            var migrationConnectionString = DatabaseHelpers.CreateApplicationDatabaseAndUser(configuration, environmentPrefix, databaseName);

            Console.WriteLine("----------------------------------------------------");
            Console.WriteLine("Creation of database and users complete");
            Console.WriteLine("----------------------------------------------------");

            Console.WriteLine("----------------------------------------------------");
            Console.WriteLine("Starting Database Migration");
            Console.WriteLine("----------------------------------------------------");

            using var context = new Program().CreateDbContext(new[] { migrationConnectionString });
            // Extend the timeout and run the migrations
            context.Database.SetCommandTimeout(180);
            context.Database.Migrate();

            Console.WriteLine("----------------------------------------------------");
            Console.WriteLine("Database Migration Complete");
            Console.WriteLine("----------------------------------------------------");
            return 0;
        }
    }
}