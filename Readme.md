## To build and use this template from source

one time command to add a new source to nuget  (replace [user] with your username)
```
nuget sources Add -Name Template -Source C:\Users\[user]\.templateengine
```
---
the following commands will work when using visual studio 2022

Ensure you don't have visual studio open (or restart after)

Run
```
dotnet pack -o C:\temp\templates-test Ucas.Template.csproj
```
Update the version of the nuget package to the one generated with the previous command
```
dotnet new install C:\temp\templates-test\Ucas.Template.ApiPostgres.Test.1.0.1.nupkg
```
To uninstall
```
dotnet new uninstall Ucas.Template.ApiPostgres.Test
```
In visual studio templates you should not see a template called ucas.api.postgres
